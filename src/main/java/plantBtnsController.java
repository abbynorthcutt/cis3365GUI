import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class plantBtnsController {
    @FXML
    public void homeButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent mainScreen = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(mainScreen));
        primaryStage.show();
    }

    @FXML
    public void customerButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent customerScreen = FXMLLoader.load(getClass().getResource("customer.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(customerScreen));
        primaryStage.show();
    }

    @FXML
    public void employeeButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent employeeScreen = FXMLLoader.load(getClass().getResource("employee.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(employeeScreen));
        primaryStage.show();
    }

    @FXML
    public void orderButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent orderScreen = FXMLLoader.load(getClass().getResource("orders.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(orderScreen));
        primaryStage.show();
    }

    @FXML
    public void productButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent productScreen = FXMLLoader.load(getClass().getResource("products.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(productScreen));
        primaryStage.show();
    }

    @FXML
    public void arrangementButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent productScreen = FXMLLoader.load(getClass().getResource("arrangement.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(productScreen));
        primaryStage.show();
    }

    @FXML
    public void reportButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent reportScreen = FXMLLoader.load(getClass().getResource("report.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(reportScreen));
        primaryStage.show();
    }

    @FXML
    public void saveBtnClicked (ActionEvent actionEvent) throws IOException {

    }

    @FXML
    public void cancelBtnClicked (ActionEvent actionEvent) throws IOException {
        Parent plantScreen = FXMLLoader.load(getClass().getResource("plant.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(plantScreen));
        primaryStage.show();
    }

}
