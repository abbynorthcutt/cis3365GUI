import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class customerController {

    @FXML
    private TableColumn<Customer,Integer> custIDCol = new TableColumn<>("ID");
    @FXML
    private TableColumn<Customer,String> custFNameCol = new TableColumn<>("First Name");
    @FXML
    private TableColumn<Customer,String> custLNameCol = new TableColumn<>("Last Name");
    @FXML
    private TableColumn<Customer,String> custPhoneCol = new TableColumn<>("Phone");
    @FXML
    private TableColumn<Customer,String> custEmailCol = new TableColumn<>("Email");
    @FXML
    public TableView<Customer> customerTable = new TableView<>();


    public void initialize() throws SQLException {
        BasicDataSource bds = new BasicDataSource();
        bds.setUsername("postgres");
        bds.setPassword("postgres");
        bds.setUrl("jdbc:postgresql://localhost:5432/flowershop");
        custIDCol.setCellValueFactory(new PropertyValueFactory<>("customerID"));
        custFNameCol.setCellValueFactory(new PropertyValueFactory<>("customerFirstName"));
        custLNameCol.setCellValueFactory(new PropertyValueFactory<>("customerLastName"));
        custPhoneCol.setCellValueFactory(new PropertyValueFactory<>("customerPhone"));
        custEmailCol.setCellValueFactory(new PropertyValueFactory<>("customerEmail"));
        QueryRunner qr = new QueryRunner(bds);
        ResultSetHandler<List<Customer>> C = new BeanListHandler<Customer>(Customer.class);
        String query = "SELECT customer.customerID,customer.customerFirstName,customer.customerLastName,customer.customerPhone,customer.customerEmail FROM customer ORDER BY customer.customerID";
        List<Customer> customer = qr.query(query, C);
        customerTable.setItems(FXCollections.observableArrayList(customer));




    }


    @FXML
    public void homeButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent mainScreen = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(mainScreen));
        primaryStage.show();
    }

    @FXML
    public void customerButtonClicked(ActionEvent actionEvent) throws IOException{
        Parent customerScreen = FXMLLoader.load(getClass().getResource("customer.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(customerScreen));
        primaryStage.show();
    }

    @FXML
    public void employeeButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent employeeScreen = FXMLLoader.load(getClass().getResource("employee.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(employeeScreen));
        primaryStage.show();
    }

    @FXML
    public void orderButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent orderScreen = FXMLLoader.load(getClass().getResource("orders.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(orderScreen));
        primaryStage.show();
    }

    @FXML
    public void productButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent productScreen = FXMLLoader.load(getClass().getResource("products.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(productScreen));
        primaryStage.show();
    }

    @FXML
    public void arrangementButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent arrangementScreen = FXMLLoader.load(getClass().getResource("arrangement.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(arrangementScreen));
        primaryStage.show();
    }

    @FXML
    public void reportButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent reportScreen = FXMLLoader.load(getClass().getResource("report.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(reportScreen));
        primaryStage.show();
    }

    @FXML
    public void addButtonClicked(ActionEvent actionEvent) throws IOException{
        Parent addCustomerScreen = FXMLLoader.load(getClass().getResource("addCustomer.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(addCustomerScreen));
        primaryStage.show();
    }

    @FXML
    public void editButtonClicked(ActionEvent actionEvent) throws IOException{
        Parent addCustomerScreen = FXMLLoader.load(getClass().getResource("addCustomer.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(addCustomerScreen));
        primaryStage.show();

    }

    @FXML
    public void deleteButtonClicked(ActionEvent actionEvent) throws IOException, SQLException{

        BasicDataSource bds = new BasicDataSource();
        bds.setUsername("postgres");
        bds.setPassword("postgres");
        bds.setUrl("jdbc:postgresql://localhost:5432/flowershop");
        Customer customerID = customerTable.getSelectionModel().getSelectedItem();
        int id = customerID.getCustomerID();
        String sql = "DELETE FROM customer WHERE customerid = "+id;
        Connection connection = bds.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.execute();
        customerTable.getItems().remove(customerID);
        preparedStatement.close();
        connection.close();

    }



}