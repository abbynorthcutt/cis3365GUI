
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.commons.dbcp2.BasicDataSource;

import java.io.IOException;
import java.sql.*;

public class CustomerBtnsController extends customerController {

    @FXML
    private TextField fNameText;
    @FXML
    private TextField lNameText;
    @FXML
    private TextField phoneText;
    @FXML
    private TextField emailText;
    @FXML
    private TextField addressText;
    @FXML
    private TextField cityText;
    @FXML
    private TextField zipText;
    @FXML
    private Button btnSave;




    @FXML
    public void homeButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent mainScreen = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(mainScreen));
        primaryStage.show();
    }

    @FXML
    public void employeeButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent employeeScreen = FXMLLoader.load(getClass().getResource("employee.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(employeeScreen));
        primaryStage.show();
    }

    @FXML
    public void orderButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent orderScreen = FXMLLoader.load(getClass().getResource("orders.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(orderScreen));
        primaryStage.show();
    }

    @FXML
    public void productButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent productScreen = FXMLLoader.load(getClass().getResource("products.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(productScreen));
        primaryStage.show();
    }

    @FXML
    public void arrangementButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent productScreen = FXMLLoader.load(getClass().getResource("arrangement.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(productScreen));
        primaryStage.show();
    }

    @FXML
    public void reportButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent reportScreen = FXMLLoader.load(getClass().getResource("report.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(reportScreen));
        primaryStage.show();
    }

    @FXML
    public void cancelBtnClicked (ActionEvent actionEvent) throws IOException {


    }

   @FXML
    public void btnSaveClicked (ActionEvent actionEvent) throws SQLException, IOException {
       BasicDataSource bsd = new BasicDataSource();
       bsd.setUsername("postgres");
       bsd.setPassword("postgres");
       bsd.setUrl("jdbc:postgresql://localhost:5432/flowershop");
       String firstNameValue = fNameText.getText();
       String lastNameValue = lNameText.getText();
       String phoneValue = phoneText.getText();
       String emailValue = emailText.getText();
       String addressValue = addressText.getText();
       String cityValue = cityText.getText();
       String zipValue = zipText.getText();
       Connection connection = bsd.getConnection();
       String query ="INSERT INTO customer(customerFirstName, customerLastName, customerPhone, customerEmail, customerAddress, customerCity, customerZip) VALUES (?, ?, ?, ?, ?, ?, ?)";
       try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
           preparedStatement.setString(1, firstNameValue);
           preparedStatement.setString(2, lastNameValue);
           preparedStatement.setString(3, phoneValue);
           preparedStatement.setString(4, emailValue);
           preparedStatement.setString(5, addressValue);
           preparedStatement.setString(6, cityValue);
           preparedStatement.setString(7, zipValue);
           preparedStatement.executeUpdate();
           preparedStatement.close();
       }catch (SQLException e) {
           e.printStackTrace();
       }
       Alert alert = new Alert(Alert.AlertType.INFORMATION);
       alert.setTitle("Successfully added Customer");
       alert.setHeaderText("Added Customer to Database");
       alert.setContentText("Successfully added Customer");
       alert.showAndWait();

       Parent customerScreen = FXMLLoader.load(getClass().getResource("customer.fxml"));
       Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
       primaryStage.setScene(new Scene(customerScreen));
       primaryStage.show();
   }






}
